
$(document).ready(function () { 
    $("#tags").keyup(function () {
        $.ajax({
            type: "POST",
            url: "{{ path('search_user') }}",
            data: {
                "tags": $("#tags").val()
            },
            dataType: "json",
            success: function (data) {				            	
                if (data.length > 0) {
                    $('#DropdownUser').empty();
                    $('#tags').attr("data-toggle", "dropdown");
                    $('#DropdownUser').dropdown('toggle');
                }
                else if (data.length == 0) {
                    $('#tags').attr("data-toggle", "");
                }
                $.each(data, function (key,value) {				                	
                   if (data.length >= 0){					                    	
                        $('#DropdownUser').append('<li class = "list-group-item" role="presentation" ><a href = "">' + value.username + '</a></li>');					                    	
                    }
                });
            }
        });					       			        		        
	});	
	$('ul.txtuser').on('click', 'li a', function () {
    	$('#tags').val($(this).text());
    });					   
});
