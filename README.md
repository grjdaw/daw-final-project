# Gamer's Guild #


Gamer's Guild is a web application, built with Symfony 3, made to simplify and unify your favourite game's statistics across different platforms.

# Installation Guide #

##Downloading Files##

First you will need to clone the project to your server, to do that run this command on your public_html directory
	
	git clone https://a14raugarsan@bitbucket.org/grjdaw/daw-final-project.git

In order to work on a stable version, make sure to be on the Production enviroment with the following command:

	 git fetch && git checkout Production

Now that the project files are fully downloaded, it's time for some configuration.


##Creating a Database##

Launch your MySQL program with your credentials:

	mysql -h host -u user -p password

and create a MySQL database 

	CREATE DATABASE databaseName;

That's all you will need, since all the tables and relations will be created by Doctrine later on.

Next, move to your project directory and search for app/config/parameters.yml, edit this file with your database information. Like so:

	# This file is auto-generated during the composer install
	parameters:
	    database_host: labs.iam.cat #your host
	    database_port: null
	    database_name: databaseName #your database name
	    database_user: user #your database username (make sure it has root privileges)
	    database_password: pasword #your password
	    mailer_transport: smtp
	    mailer_host: 127.0.0.1
	    mailer_user: null
	    mailer_password: null
	    secret: ThisTokenIsNotSoSecretChangeIt #changing this is advisable, as the default secret is not really secret

Lastly, you'll need to tell symfony to generate the tables for you

	php bin/console doctrine:schema:update --force


## Configuring Required Bundles ##

In order for Symfony bundles to work, they need to be installed. Dependencies are already listed in the composer.json file  so the only command needed is:

	composer install


Your web application should now be accesible under the following URL (keep in mind it may vary with different directory structures)

	http://yourHostAddress.com/web