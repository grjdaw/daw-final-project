<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="Users")
 *
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */
 
class User extends BaseUser {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     */
    protected $birth;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $firstName;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $lastName;
     
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $nationality;
     
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $about;
     
    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/png" },
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    public $avatar;

    protected $tempAvatarPath;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $avatarUpdatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $avatarPath;
     
     /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $steamID;
     
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $psnID;
     
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $xboxID;
    
    /**
     * @ORM\ManyToMany(targetEntity="Game")
     * @ORM\JoinTable(name="users_games",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="platform_id")}
     *      )
     */
    protected $games;
    
    public function __construct() {
        parent::__construct();
		$this->games = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return User
     */
    public function setBirth($birth) {
        $this->birth = $birth;
        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth() {
        return $this->birth;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set nationality
     *
     * @param string $nationality
     *
     * @return User
     */
    public function setNationality($nationality) {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * Get nationality
     *
     * @return string
     */
    public function getNationality() {
        return $this->nationality;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about) {
        $this->about = $about;
        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout() {
        return $this->about;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
		$this->avatar = $file;
        if (is_file($this->getAbsolutePath())) {
            $this->tempAvatarPath = $this->avatarPath;
            $this->avatarPath = null;
        } else {
            $this->avatarPath = 'initial';
        }

        return $this;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
		 return $this->avatar;
	}

    /**
     * Set avatarUpdatedAt
     *
     * @param \DateTime $avatarUpdatedAt
     *
     * @return User
     */
    public function setAvatarUpdatedAt($avatarUpdatedAt) {
        $this->avatarUpdatedAt = $avatarUpdatedAt;
        return $this;
    }

    /**
     * Get avatarUpdatedAt
     *
     * @return \DateTime
     */
    public function getAvatarUpdatedAt() {
        return $this->avatarUpdatedAt;
    }
	
	/**
     * Set avatarPath
     *
     * @param string $avatarPath
     *
     * @return User
     */
    public function setAvatarPath($avatarPath) {
        $this->avatarPath = $avatarPath;

        return $this;
    }

    /**
     * Get avatarPath
     *
     * @return string
     */
    public function getAvatarPath() {
        return $this->avatarPath;
    }

    /**
     * Set steamID
     *
     * @param string $steamID
     *
     * @return User
     */
    public function setSteamID($steamID) {
        $this->steamID = $steamID;
        return $this;
    }

    /**
     * Get steamID
     *
     * @return string
     */
    public function getSteamID() {
        return $this->steamID;
    }

    /**
     * Set psnID
     *
     * @param string $psnID
     *
     * @return User
     */
    public function setPsnID($psnID) {
        $this->psnID = $psnID;
        return $this;
    }

    /**
     * Get psnID
     *
     * @return string
     */
    public function getPsnID() {
        return $this->psnID;
    }

    /**
     * Set xboxID
     *
     * @param string $xboxID
     *
     * @return User
     */
    public function setXboxID($xboxID) {
        $this->xboxID = $xboxID;
        return $this;
    }

    /**
     * Get xboxID
     *
     * @return string
     */
    public function getXboxID() {
        return $this->xboxID;
    }

    /**
     * Add game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return User
     */
    public function addGame(\AppBundle\Entity\Game $game) {
        $this->games[] = $game;
        return $this;
    }

    /**
     * Remove game
     *
     * @param \AppBundle\Entity\Game $game
     */
    public function removeGame(\AppBundle\Entity\Game $game) {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames() {
        return $this->games;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            $this->setAvatarPath($this->getFile()->guessExtension());
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload () {
		if (null === $this->getFile()) {
			return;
		}

        if (isset($this->tempAvatarPath)) {
            // delete the old image
            unlink($this->tempAvatarPath);
            // clear the temp image path
            $this->tempAvatarPath = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->username.'.'.$this->getFile()->guessExtension()
        );

        $this->setFile(null);
	}

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove() {
        $this->tempAvatarPath = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        $file = $this->getAbsolutePath();
        if (isset($this->tempAvatarPath)) {
            unlink($this->tempAvatarPath);
        }
    }

    public function getAbsolutePath() {
        return null === $this->avatarPath
            ? null
            : $this->getUploadRootDir().'/'.$this->username.'.'.$this->avatarPath;
    }

    public function getWebPath() {
        return null === $this->avatarPath
            ? null
            : $this->getUploadDir().'/'.$this->avatarPath;
    }

    protected function getUploadRootDir() {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir() {
        return 'images/uploads';
    }
}
