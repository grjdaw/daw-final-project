<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AchievementRepository")
 * @ORM\Table(name="Achievements")
 *
 * @author Galo Drouet  <@gmail.com>
 * @author Jordi Guerrero <@gmail.com>
 * @author Raul Garcia <@gmail.com>
 */
 
class Achievement {
	
	/**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /** 
     * @ORM\Column(type="string", unique=true)
     */
    private $platformID;
    
    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="achievements")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="platform_id")
     */
    private $gameID;
    
    /**
     * @ORM\Column(type="string")
     */
    private $name;
     
    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Achievement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Achievement
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set gameID
     *
     * @param \AppBundle\Entity\Game $gameID
     *
     * @return Achievement
     */
    public function setGameID(\AppBundle\Entity\Game $gameID = null) {
        $this->gameID = $gameID;
        return $this;
    }

    /**
     * Get gameID
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGameID() {
        return $this->gameID;
    }

    /**
     * Set platformID
     *
     * @param string $platformID
     *
     * @return Achievement
     */
    public function setPlatformID($platformID)
    {
        $this->platformID = $platformID;

        return $this;
    }

    /**
     * Get platformID
     *
     * @return string
     */
    public function getPlatformID()
    {
        return $this->platformID;
    }
}
