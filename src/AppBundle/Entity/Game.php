<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 * @ORM\Table(name="Games")
 *
 * @author Galo Drouet  <@gmail.com>
 * @author Jordi Guerrero <@gmail.com>
 * @author Raul Garcia <@gmail.com>
 */
 
class Game {
	
	/**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /** 
     * @ORM\Column(type="string", unique=true)
     */
    private $platformID;
     
    /** 
     * @ORM\Column(type="string")
     */
    private $platform;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     * @Assert\Range(
     *     min = 0,
     *     max = 10,
     *     minMessage = "Min is 0",
     *     maxMessage = "Max is 10"
     * )
     */
    private $score;
    
    /**
     * @ORM\OneToMany(targetEntity="Achievement", mappedBy="gameID")
     */
    private $achievements;
    
    public function __construct() {
		$this->achievements = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Game
     */
    public function setPlatform($platform) {
        $this->platform = $platform;
        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform() {
        return $this->platform;
    }

    /**
     * Set score
     *
     * @param string $score
     *
     * @return Game
     */
    public function setScore($score) {
        $this->score = $score;
        return $this;
    }

    /**
     * Get score
     *
     * @return string
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * Add achievement
     *
     * @param \AppBundle\Entity\Achievement $achievement
     *
     * @return Game
     */
    public function addAchievement(\AppBundle\Entity\Achievement $achievement) {
        $this->achievements[] = $achievement;
        return $this;
    }

    /**
     * Remove achievement
     *
     * @param \AppBundle\Entity\Achievement $achievement
     */
    public function removeAchievement(\AppBundle\Entity\Achievement $achievement) {
        $this->achievements->removeElement($achievement);
    }

    /**
     * Get achievements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAchievements() {
        return $this->achievements;
    }

    /**
     * Set platformID
     *
     * @param string $platformID
     *
     * @return Game
     */
    public function setPlatformID($platformID)
    {
        $this->platformID = $platformID;

        return $this;
    }

    /**
     * Get platformID
     *
     * @return string
     */
    public function getPlatformID()
    {
        return $this->platformID;
    }
}
