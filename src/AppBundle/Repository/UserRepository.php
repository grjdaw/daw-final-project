<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class UserRepository extends EntityRepository {

	public function findByLetters($string){
        return $this->getEntityManager()->createQuery('SELECT u FROM AppBundle:User u  
                WHERE u.username LIKE :string')
                ->setParameter('string','%'.$string.'%')
                ->getResult();
    }
}
