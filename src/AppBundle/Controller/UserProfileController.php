<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use AppBundle\Entity\User;

/**
 * Controller managing the user's profile
 *
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class UserProfileController extends Controller{
  
    /**
    * @Route("/profile/user/{username}", name="profileUser")
    */
    public function loadUserProfile($username){       
        
        //Get User by Username
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);

        //Get current User
        $currentUser = $this->getUser();

        if(!$user) return $this->redirect($this->generateUrl('notFoundAction', array('type' => "userid")));  
        
        //Array of phrases for the games that the player has not played.
        $gameNotPlayedPhrases = array("Priorities, ".$user->getUsername().", priorities.",
            "KILL DANTE!");

        //Api Keys
        $steamApiKey = "555C7055C871950422E9168F99B7943F";
        $giantBombApiKey = "7197fe050d07e2f6c699741948d127aeae763511";
        
        //Final Array Games
        $arraySteamGames = null;
        $arrayPSNGames = null;
        $arrayTotalXboxGames = null;

        //Load Steam Games
        if($user->getSteamID() != null) {
            $steamId = $user->getSteamID();
            $getOwnedGames = @file_get_contents("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . $steamApiKey . "&steamid=" . $steamId . "&format=json&include_appinfo=1&&include_played_free_games=1");
            $ownedGames = json_decode($getOwnedGames, true);

            if($ownedGames['response']['game_count'] != 0) {
                $arrayImgSteam = array();
                $arraySteamGames = $ownedGames['response']['games'];               
            } else {
                $arraySteamGames = array();
            }          
        }


        //Load PSN Games
        if($user->getPsnID() != null) {
            $psnId = $user->getPsnID();

            $authorization = @file_get_contents("http://a14galdrobej.ga/PSNExample.php");
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header'  => "Authorization: $authorization"
                    )
                ));

            $getOwnedGames = @file_get_contents("https://es-tpy.np.community.playstation.net/trophy/v1/trophyTitles?fields=%40default&npLanguage=en&platform=PS3%2CPSVITA%2CPS4&comparedUser=".$psnId, false, $context);
            $ownedGames = json_decode($getOwnedGames, true);
            
            if($ownedGames['totalResults'] > 0) {
                $trophyTitles = $ownedGames['trophyTitles'];
                $arrayPSNGames = array();
                foreach($trophyTitles as $game) {
                    if(array_key_exists('comparedUser', $game)) {
                        array_push($arrayPSNGames, $game);
                    }
                }  
            } else {
                $arrayPSNGames = array();
            }    
        }

        //Load Xbox Games
        if($user->getXboxID() != null) {
            $xboxId = $user->getXboxID();

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header' => "X-Auth: 682706873075be49a641026c46da7b29acbb53c7"
                    )
                ));

            $getOwnedGames360 = @file_get_contents("https://xboxapi.com/v2/".$xboxId."/xbox360games", false, $context);
            $ownedGames360 = json_decode($getOwnedGames360, true);

            $getOwnedGamesOne = @file_get_contents("https://xboxapi.com/v2/".$xboxId."/xboxonegames", false, $context);
            $ownedGamesOne = json_decode($getOwnedGamesOne, true);

            if($ownedGames360['pagingInfo']['totalRecords'] > 0) {
                $arrayXbox360Games = array(); 
                foreach($ownedGames360['titles'] as $game) {
                    if($game['titleType'] != 5) {
                        array_push($arrayXbox360Games, $game);
                    }
                }
            } else {
                $arrayXbox360Games = array();
            }

            if($ownedGamesOne['pagingInfo']['totalRecords'] > 0) {
                $arrayXboxOneGames = array(); 
                foreach($ownedGamesOne['titles'] as $game) {
                    if($game['titleType'] != 5) {
                        array_push($arrayXboxOneGames, $game);
                    }
                }  
            } else {
                $arrayXboxOneGames = array();
            }

            $arrayTotalXboxGames = array_merge($arrayXboxOneGames, $arrayXbox360Games);
        }

        //Check if you search yourself
        if($currentUser->getUsername() != $username){
            return $this->render('FOSUserBundle:Profile:showUser.html.twig', array('user'=>$user, 'steamGames' => $arraySteamGames, 'psnGames' => $arrayPSNGames, 'xboxGames' => $arrayTotalXboxGames, "randomPhrases" => $gameNotPlayedPhrases,));
        }         
        else return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }
}