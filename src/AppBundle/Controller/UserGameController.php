<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing the game profile
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class UserGameController extends Controller {
	
    //Load Game Profile
    /**
     * @Route("/game/{username}/{id}", name="gameUserProfile")
     */
    public function gameUserProfile($username, $id) {

        //Get user by username
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        
        //Get current user
        $currentUser = $this->getUser();

        //Check if user exist
        if(!$user) return $this->redirect($this->generateUrl('notFoundAction', array('type' => "user")));

        //Check if user is you
        if($user->getUsername() == $currentUser->getUsername()) return $this->redirect($this->generateUrl('gameProfile', array('id' => $id)));
        
        //Get general information about a game(GiantBomb API)
        $finalGameID = null;
        $options = array(
          'http'=>array(
             'method'=>"GET",
             'header'=>"Accept-language: en\r\n".
             "Cookie: foo=bar\r\n".
             "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
             )
          );
        $context = stream_context_create($options);
        $giantBombInfo = @file_get_contents("http://www.giantbomb.com/api/game/".$id."/?api_key=7197fe050d07e2f6c699741948d127aeae763511&format=json&field_list=name,image,original_release_date,developers,genres,platforms", false, $context);
        if(!$giantBombInfo) return $this->redirect($this->generateUrl('notFoundAction', array('type' => "game")));
        $info = json_decode($giantBombInfo, true);

        //Game name for URL's
        if(!array_key_exists('name', $info['results'])) return $this->redirect($this->generateUrl('notFoundAction', array('type' => "game")));
        $nameGame = $info['results']['name'];
        $normalNameGame = str_replace(' ', '+', $nameGame);

        //Control of platforms
        $ps4 = false;
        $ps3 = false;
        $vita = false;
        $pc = false;
        foreach($info['results']['platforms'] as $platform) {
            if($platform['abbreviation'] == "PS4") {
                $ps4 = true;
            } else if ($platform['abbreviation'] == "PS3") {
                $ps3 = true;
            } else if($platform['abbreviation'] == "VITA" || $platform['abbreviation'] == "PSNV") {
                $vita = true;
            } else if($platform['abbreviation'] == "PC") {
                $pc = true;
            }
        }

        //Platforms ID's
        $psnId = null;
        $steamId = null;

        //Arrays of trophies
        $psnTrophiesGame = array();
        $steamTrophiesGame = array();

        //Check if user have psn id
        if($user->getPsnID() != null) {
            $psnId = $user->getPsnID();
            $psnTrophiesGame = $this->getPSNTrophies($ps4, $ps3, $vita, $normalNameGame, $psnId);
        }

        //Check if user have steam id
        if($user->getSteamID() != null) {
            $steamId = $user->getSteamID();
            $steamTrophiesGame = $this->getSteamTrophies($pc, $normalNameGame, $steamId);
        }

        if($psnId == null && $steamId == null){
            return $this->render('gamersguild/game/show_content.html.twig', array('game' => $info['results'], 'psnTrophies' => $psnTrophiesGame, 'steamTrophies' =>  $steamTrophiesGame));
        } else if($psnId != null && $steamId == null) {
            return $this->render('gamersguild/game/show_content.html.twig', array('game' => $info['results'], 'psnTrophies' => $psnTrophiesGame, 'steamTrophies' =>  $steamTrophiesGame, 'psn'=>true));
        } else if($psnId == null && $steamId != null) {
            return $this->render('gamersguild/game/show_content.html.twig', array('game' => $info['results'], 'psnTrophies' => $psnTrophiesGame, 'steamTrophies' =>  $steamTrophiesGame, 'steam'=>true));
        } else {
            return $this->render('gamersguild/game/show_content.html.twig', array('game' => $info['results'], 'psnTrophies' => $psnTrophiesGame, 'steamTrophies' =>  $steamTrophiesGame, 'steam'=>true, 'psn'=>true));
        }
    }

    /**
     * Get PSN Trophies by game title
     *
     * @param Integer $ps4 (Available for PS4?)
     * @param Integer $ps3 (Available for PS3?)
     * @param Integer $vita (Available for Vita?)
     * @param String $normalNameGame (Game Title)
     * @param String $psnId (User PSN ID)
     *
     * @return Array $psnTrophiesGame (Array with the game trophies in PSN)
     */
    public function getPSNTrophies($ps4, $ps3, $vita, $normalNameGame, $psnId) {
        //Final Game ID in PSN
        $finalGameID = null;

        //Array of game trophies
        $psnTrophiesGame = array();

        //Check if the game is available in PSN
        if($ps4 || $ps3 || $vita){

            //Depending on the platform it will be call one or other URL(Web Scraping)
            if($ps4 && $ps3 && !$vita){
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=1&ps3=1&vita=0&gb=".$normalNameGame);
            } else if($ps4 && !$ps3 && !$vita) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=1&ps3=0&vita=0&gb=".$normalNameGame);
            } else if($ps4 && !$ps3 && $vita) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=1&ps3=0&vita=1&gb=".$normalNameGame);
            } else if($ps3 && !$ps4 && !$vita) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=0&ps3=1&vita=0&gb=".$normalNameGame);
            } else if($ps3 && !$ps4 && $vita) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=0&ps3=1&vita=1&gb=".$normalNameGame);
            } else if($vita && !$ps3 && !$ps4) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=0&ps3=0&vita=1&gb=".$normalNameGame);
            } else if($vita && $ps3 && $ps4) {
                $psnIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSPSN.php?ps4=1&ps3=1&vita=1&gb=".$normalNameGame);
            }

            //Array with2 possible ID's for the game
            $psnIDS = json_decode($psnIDArray, true);
            
            if(!empty($psnIDS)) {

                //Script to refresh de PSN suthorization token
                $authorization = @file_get_contents("http://a14galdrobej.ga/PSNExample.php");

                //Get your Games
                $context = stream_context_create(array(
                    'http' => array(
                        'method' => 'GET',
                        'header'  => "Authorization: $authorization"
                        )
                    ));    
                $getOwnedGames = @file_get_contents("https://es-tpy.np.community.playstation.net/trophy/v1/trophyTitles?fields=%40default&npLanguage=en&platform=PS3%2CPSVITA%2CPS4&comparedUser=".$psnId, false, $context);
                $ownedGames = json_decode($getOwnedGames, true);

                //Check if you have games
                if($ownedGames['totalResults'] > 0) {
                    //Search for a match between the array of possible id's and the id's of your games
                    $trophyTitles = $ownedGames['trophyTitles'];
                    foreach($psnIDS as $npCommunicationId) {
                        foreach($trophyTitles as $game) {
                            if(array_key_exists('comparedUser', $game)) {
                                if($game['npCommunicationId'] == $npCommunicationId) {
                                    $finalGameID = $npCommunicationId;
                                    break;
                                }
                            }
                        }

                        if($finalGameID != null) {
                            break;
                        }
                    }

                    if($finalGameID == null) {
                        //If there is not matches, it is assumed that the first of the possibles is the correct one
                        $finalGameID = $psnIDS[0];
                    }

                } else {
                    //If you don't have any games it is assumed that the first of the possibles is the correct one
                    $finalGameID = $psnIDS[0];
                }

                //Get the trophies
                $getTrophyGame = @file_get_contents("https://es-tpy.np.community.playstation.net/trophy/v1/trophyTitles/".$finalGameID."/trophyGroups/all/trophies?fields=%40default&npLanguage=en&comparedUser=".$psnId, false, $context);
                $psnTrophiesGame = json_decode($getTrophyGame, true);
            } else {
                $psnTrophiesGame = array("hasNotTrophies" => true);
            }
        }

        return $psnTrophiesGame;
    }

    /**
     * Get Steam Trophies by game title
     *
     * @param Integer $pc (Available for PC?)
     * @param String $normalNameGame (Game Title)
     * @param String $steamId (User Steam ID)
     *
     * @return Array $steamTrophiesGame (Array with the game trophies in STEAM)
     */
    public function getSteamTrophies($pc, $normalNameGame, $steamId){
        //Final Game ID in PSN
        $finalGameID = null;

        //Array of game trophies
        $steamTrophiesGame = array();

        //Check if the game is available in PC
        if($pc) {
            
            //Api Key
            $steamApiKey = "555C7055C871950422E9168F99B7943F";

            //Web Scraping: return an array of possible id's by a game title
            $steamIDArray = @file_get_contents("http://a14galdrobej.ga/WS/WSSteam.php?gb=".$normalNameGame);
            $steamIDS = json_decode($steamIDArray, true);

            //Check if the web scraping script return something
            if(!empty($steamIDS)) {

                //Get your Games
                $getOwnedGames = @file_get_contents("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=".$steamApiKey
                    ."&steamid=". $steamId
                    ."&format=json&include_played_free_games=1&appids_filter[0]=".$steamIDS[0]."&appids_filter[1]=".$steamIDS[1]);
                $ownedGames = json_decode($getOwnedGames, true);

                //Check if you have games
                if($ownedGames['response']['game_count'] != 0) {
                    //Search for a match between the array of possible id's and the id's of your games
                    $arraySteamGames = $ownedGames['response']['games'];
                    foreach ($steamIDS as $appid) {
                        foreach ($arraySteamGames as $game) {
                            if($game['appid'] == $appid){
                                $finalGameID = $appid;
                                break;
                            }
                        }

                        if($finalGameID != null) {
                            break;
                        }
                    }
                    
                    //Get the trophies(The URL depends on whether you have it or not)
                    if($finalGameID == null) {
                        $finalGameID = $steamIDS[0];                
                        $getTrophyGame = @file_get_contents("http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=".$steamApiKey."&appid=".$finalGameID);;
                        $steamTrophiesGame = json_decode($getTrophyGame, true);
                    } else {
                        $getTrophyGame = @file_get_contents("http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=".$finalGameID."&key=".$steamApiKey."&steamid=".$steamId."&l=ENG");
                        $steamTrophiesGame = json_decode($getTrophyGame, true);
                    }

                } else {
                    //If you don't have any games it is assumed that the first of the possibles is the correct one
                    $finalGameID = $steamIDS[0];
                    $getTrophyGame = @file_get_contents("http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=".$steamApiKey."&appid=".$finalGameID);
                    $steamTrophiesGame = json_decode($getTrophyGame, true);
                }

                // At this point if the array is empty means that the game is for Steam but has no trophies
                if(empty($steamTrophiesGame)) {
                    $steamTrophiesGame = array("playerstats" => false, "game" => false, "hasNotTrophies" => true);
                }
            }
        }

        return $steamTrophiesGame;
    }
}
