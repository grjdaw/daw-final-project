<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing the news
 *
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 */

class RankingController extends Controller {

    /**
     * @Route("/ranking", name="ranking")
     */
    public function indexAction() {
		
		return $this->render('gamersguild/ranking.html.twig');
    }
}

