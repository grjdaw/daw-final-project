<?php

namespace AppBundle\Controller\MobileApi;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class RegisterController extends Controller{
    /**
     * @Route("/api/v1/register", name="loginMobile")
     */
    public function indexAction(Request $request){
        
        $data = $request->request->all();
        $username = $data['username'];
        $userPassword = $data['password'];

        $em = $this->getDoctrine()->getManager();            
        $usuari = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if(!$usuari) throw $this->createNotFoundException("User not found");

        if($usuari->getPassword() == $userPassword) return new Response(json_encode($usuari));

        return new Response("");
    }
    
    /*public function registerAction(){}
        $user = new User();

        $request  = $this->getRequest();
        $username = $request->request->get('username');
        $password= $request->request->get('password');
        $email= $request->request->get('email');

      $factory = $this->get('security.encoder_factory'); 

      $encoder = $factory->getEncoder($user); 
      $password = $encoder->encodePassword($password, $user->getSalt()); 
      $user->setPassword($password); 


        $user->setUsername($username);
        $user->setUsernameCanonical($username);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setEnabled(true);
        $user->setLocked(false);
        $user->setExpired(false);

        $user->setCredentialsExpired(false);
        $em = $this->get('doctrine')->getEntityManager();
        $em->persist($user);
        $em->flush();
        //$response = new Response(json_encode(array('user' => $tes)));
        //$response->headers->set('Content-Type', 'application/json');

        //return $response;
          return new JsonResponse('good');
    }*/

}
