<?php

namespace AppBundle\Controller\MobileApi;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class LoginController extends Controller{
    /**
     * @Route("/api/v1/login", name="loginMobile")
     */
    public function indexAction(Request $request){
        
        $data = $request->request->all();
        $username = $data['username'];
        $userPassword = $data['password'];

        $em = $this->getDoctrine()->getManager();            
        $usuari = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if(!$usuari) throw $this->createNotFoundException("User not found");

        if($usuari->getPassword() == $userPassword) return new Response(json_encode($usuari));

        return new Response("");
    }
}
