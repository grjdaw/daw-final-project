<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Game;
use AppBundle\Entity\Achievement;
use AppBundle\Entity\User;

/**
 * Controller managing the several Apis thas we used
 *
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class ApiController extends Controller {

    /**
     * Add Steam ID to the DB
     *
     * @param Request $request(steamID)
     */
    /**
     * @Route("/profile/addSteam", name="add_steamID")
     */
    public function addSteamIDAction(Request $request) {
        $user = $this->getUser();
        $data = $request->request->all();
        $steamApiKey = "555C7055C871950422E9168F99B7943F";

        if($data['form']['type'] == "steamid") {
            $getOwnedGames = @file_get_contents("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . $steamApiKey . "&steamid=" . $data['form']['steamID'] . "&format=json&include_played_free_games=1");
            if($getOwnedGames != null) {
                $user = $this->getUser();
                $user->setSteamID($data['form']['steamID']);
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);
                return $this->redirect($this->generateUrl('fos_user_profile_show'));
            } else {
                return $this->redirect($this->generateUrl('notFoundAction', array('type' => "userid")));
            }
        } else {
            $getSteamId = @file_get_contents("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=" . $steamApiKey . "&vanityurl=". $data['form']['steamID']);
            $json_a = json_decode($getSteamId, true);
            if($json_a['response']['success'] == 1) {
                $steamId = $json_a['response']['steamid'];
                $user = $this->getUser();
                $user->setSteamID($steamId);
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);
                return $this->redirect($this->generateUrl('fos_user_profile_show'));
            } else {
                return $this->redirect($this->generateUrl('notFoundAction', array('type' => "userid")));
            }
        }
    }

    /**
     * Add PSN ID to the DB
     *
     * @param Request $request(psnID)
     */
    /**
     * @Route("/profile/addPSN", name="add_psnID")
     */
    public function addPSNIDAction(Request $request) {
        $user = $this->getUser();
        $data = $request->request->all();

        $authorization = @file_get_contents("http://a14galdrobej.ga/PSNExample.php");

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header'  => "Authorization: $authorization"
                )
            ));

        $getOwnedGames = @file_get_contents("https://es-tpy.np.community.playstation.net/trophy/v1/trophyTitles?fields=%40default&npLanguage=en&platform=PS3%2CPSVITA%2CPS4&comparedUser=".$data['form']['psnID'], false, $context);
        $ownedGames = json_decode($getOwnedGames, true);

        if($ownedGames != null) {
            $user = $this->getUser();
            $user->setPsnID($data['form']['psnID']);
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        } else {
            return $this->redirect($this->generateUrl('notFoundAction', array('type' => "userid")));
        } 
    }

    /**
     * Add Xbox ID to the DB
     *
     * @param Request $request(xboxID)
     */
    /**
     * @Route("/profile/addXbox", name="add_xboxID")
     */
    public function addXboxIDAction(Request $request) {
        $user = $this->getUser();
        $data = $request->request->all();

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => "X-Auth: 682706873075be49a641026c46da7b29acbb53c7"
                )
            ));

        $getXUID = @file_get_contents("https://xboxapi.com/v2/xuid/".$data['form']['xboxID'], false, $context); 
        $getOwnedGames360 = @file_get_contents("https://xboxapi.com/v2/".$getXUID."/xbox360games", false, $context);
        $ownedGames360 = json_decode($getOwnedGames360, true);

        if($ownedGames360 != null) {
            $user = $this->getUser();
            $user->setXboxID($getXUID);
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        } else {
            $getOwnedGamesOne = @file_get_contents("https://xboxapi.com/v2/".$getXUID."/xboxonegames", false, $context);
            $ownedGamesOne = json_decode($getOwnedGamesOne, true);
            if($ownedGamesOne != null) {
                $user = $this->getUser();
                $user->setXboxID($getXUID);
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);
                return $this->redirect($this->generateUrl('fos_user_profile_show'));
            } else {
                return $this->redirect($this->generateUrl('notFoundAction', array('type' => "userid")));
            }
        } 

        return $this->redirect($this->generateUrl('fos_user_profile_show'));
    }

    /**
     * Route that found the game id of GiantBomb by the title. Redirect to the Game Profile. (Current User)
     */
    /**
     * @Route("/searchGame", name="searchNameGiantBomb")
     */
    public function searchNameGiantBomb() {
        
        $giantBombApiKey = "7197fe050d07e2f6c699741948d127aeae763511";
        $options = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"Accept-language: en\r\n".
            "Cookie: foo=bar\r\n".
            "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
            )
          );
        $context = stream_context_create($options);

        $finalName = str_replace("Trophies", "", $_GET['name']);

        $giantBombInfo = @file_get_contents("http://www.giantbomb.com/api/search/?api_key=".$giantBombApiKey."&format=json&query='".$finalName."'&resources=game&limit=1&field_list=api_detail_url", false, $context);
        $info = json_decode($giantBombInfo, true);
        
        if($info == null) {
            return $this->redirect($this->generateUrl('notFoundAction', array('type' => "game")));
        } else {
            $idGB = substr($info['results'][0]['api_detail_url'], 34, -1);
        }

        return $this->redirect($this->generateUrl('gameProfile', array('id' => $idGB)));
    }

    /**
     * Route that found the game id of GiantBomb by the title. Redirect to the Game Profile. (User)
     */
    /**
     * @Route("/searchGame/{username}", name="searchUserNameGiantBomb")
     */
    public function searchUserNameGiantBomb($username) {
        
        $giantBombApiKey = "7197fe050d07e2f6c699741948d127aeae763511";
        $options = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"Accept-language: en\r\n".
            "Cookie: foo=bar\r\n".
            "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"
            )
          );
        $context = stream_context_create($options);

        $finalName = str_replace("Trophies", "", $_GET['name']);

        $giantBombInfo = @file_get_contents("http://www.giantbomb.com/api/search/?api_key=".$giantBombApiKey."&format=json&query='".$finalName."'&resources=game&limit=1&field_list=api_detail_url", false, $context);
        $info = json_decode($giantBombInfo, true);
        
        if($info == null) {
            return $this->redirect($this->generateUrl('notFoundAction', array('type' => "game")));
        } else {
            $idGB = substr($info['results'][0]['api_detail_url'], 34, -1);
        }

        return $this->redirect($this->generateUrl('gameUserProfile', array('username' => $username,'id' => $idGB)));
    }

    /**
     * Route that redirect to a Not Found Page.
     *
     * @param String $type (Type of error)
     */
    /**
     * @Route("/notFound={type}", name="notFoundAction", defaults={"type" = ""})
     */
     public function notFoundAction($type) {
        if($type == ""){
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render('gamersguild/notFound.html.twig', array('type'=>$type));
    }

    //Alternative functon that save the games and the achievements in the DB.
    /*
    public function addSteamID() {
        $user = $this->getUser();
        $userID = $user->getId();

        $steamApiKey = "555C7055C871950422E9168F99B7943F";

        $getSteamId = @file_get_contents("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=" . $steamApiKey . "&vanityurl=". $user->getSteamID());
        $json_a = json_decode($getSteamId, true);

        if($json_a['response']['success'] == true) {

            $steamId =  $json_a['response']['steamid'];

            $getOwnedGames = @file_get_contents("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . $steamApiKey . "&steamid=" . $steamId . "&format=json&include_appinfo=1&&include_played_free_games=1");
            $ownedGames = json_decode($getOwnedGames, true);

            if($ownedGames['response']['game_count'] != 0) {

                $em = $this->getDoctrine()->getManager();

                foreach ($ownedGames['response']['games'] as $game) {

                    $gamedb = new Game();
                    $gamedb->setName($game['name']);
                    $gamedb->setPlatformID("ST".$game['appid']);
                    $gamedb->setPlatform("Steam");
                    $gamedb->setScore(0);

                    $steamPlatformID = $game['appid'];
                    $finalSteamPlatformID = "ST".$game['appid'];

                    $entity = $em->getRepository('AppBundle:Game')->findOneBy(array('platformID' => $finalSteamPlatformID, 'platform' => "Steam"));
                    if ($entity == null) { 
                        $em->persist($gamedb);
                        $em->flush();

                        $getPlayerAchievements = @file_get_contents("http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=" . $steamPlatformID . "&key=" . $steamApiKey . "&steamid=" . $steamId . "&l=ENG");
                        $PlayerAchievements = json_decode($getPlayerAchievements, true);
                        
                        if($PlayerAchievements['playerstats']['success'] == true && isset($PlayerAchievements['playerstats']['achievements'])) {
                            foreach ($PlayerAchievements['playerstats']['achievements'] as $achievement) {
                                $achievementdb = new Achievement();
                                $achievementdb->setName($achievement['name']);
                                $achievementdb->setDescription($achievement['description']);
                                $achievementdb->setGameID($gamedb);
                                $finalAchievementID = "ST".$steamPlatformID."-".$achievement['apiname'];
                                $achievementdb->setPlatformID($finalAchievementID);
                                $em->persist($achievementdb);
                                $em->flush();

                                if($achievement['achieved'] == 1) {
                                    $queryAchievement = "INSERT INTO users_games_achievements(user_id, game_id, achievement_id) VALUES ($userID, $finalSteamPlatformID, $finalAchievementID)";
                                    $em3 = $this->getDoctrine()->getManager()->getConnection()->prepare($queryAchievement);
                                    $em3->execute();
                                }
                            }
                        }
                    }

                    $query = "INSERT IGNORE INTO users_games(user_id, game_id) VALUES ($userID, $finalSteamPlatformID)";
                    $em2 = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
                    $em2->execute();
                }
            }

            $user->setSteamID("musgo93");
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
        }

        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
            'user' => $user
            ));
    }*/
}
