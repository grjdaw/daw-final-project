<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing the landing page
 * 
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class LandingController extends Controller {

    /**
     * @Route("/", name="landing_page")
     */
    public function indexAction() {
    	return $this->render('gamersguild/landing.html.twig');
    }
}
