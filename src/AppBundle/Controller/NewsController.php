<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller managing the news
 *
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 */

class NewsController extends Controller {

    /**
     * @Route("/news", name="news_page")
     */
    public function indexAction() {
    	$posts = $this->getDoctrine()
			->getRepository('AppBundle:Post')
			->findAll();
			
		if (!$posts) {
			throw $this->createNotFoundException("No news to display, sorry :(");
		}

		return $this->render('gamersguild/news.html.twig', array('posts' => $posts));
    }
}

