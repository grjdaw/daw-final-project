<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use AppBundle\Entity\User;

/**
 * Controller managing the users search
 * 
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class SearchController extends Controller{

    /**
     * @Route("/profile/searchUser", name="search_user")
     */
    public function liveSearchAction(Request $request){        
        $string = $request->request->get('tags');
       
        $users = $this->getDoctrine()
                     ->getRepository('AppBundle:User')
                     ->findByLetters($string);

        //Return users on json format
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);     

        
        $jsonContent = $serializer->serialize($users, 'json');
        $response = new Response($jsonContent);
       
        return $response;
    }

    /**
    * @Route("/profile/searchUser/profileUsername", name="search_user_get_username")
    */
    public function liveSearchActionById(Request $request){
        $username = $_GET['tags'];
        return $this->redirect($this->generateUrl('profileUser', array('username' => $username)));
    }
}