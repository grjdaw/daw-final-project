<?php

namespace UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Controller managing the current user profile
 *
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class ProfileController extends BaseController {

    //Load the profile
    public function showAction() {

        //Get current User
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        //Array of phrases for the games that the player has not played.
        $gameNotPlayedPhrases = array("Priorities, ".$user->getUsername().", priorities.", 
			"KILL DANTE!");

        //Api Keys
        $steamApiKey = "555C7055C871950422E9168F99B7943F";
        $giantBombApiKey = "7197fe050d07e2f6c699741948d127aeae763511";
        
        //Final Array Games
        $arraySteamGames = null;
        $arrayPSNGames = null;
        $arrayTotalXboxGames = null;


        //Load Steam Games
        if($user->getSteamID() != null) {
            $steamId = $user->getSteamID();
            $getOwnedGames = @file_get_contents("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . $steamApiKey . "&steamid=" . $steamId . "&format=json&include_appinfo=1&&include_played_free_games=1");
            $ownedGames = json_decode($getOwnedGames, true);

            if($ownedGames['response']['game_count'] != 0) {
                $arraySteamGames = $ownedGames['response']['games'];
            } else {
                $arraySteamGames = array();
            }          
        }

        //Load PSN Games
        if($user->getPsnID() != null) {
            $psnId = $user->getPsnID();

            $authorization = @file_get_contents("http://a14galdrobej.ga/PSNExample.php");
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header'  => "Authorization: $authorization"
                    )
                ));

            $getOwnedGames = @file_get_contents("https://es-tpy.np.community.playstation.net/trophy/v1/trophyTitles?fields=%40default&npLanguage=en&platform=PS3%2CPSVITA%2CPS4&comparedUser=".$psnId, false, $context);
            $ownedGames = json_decode($getOwnedGames, true);

            if($ownedGames['totalResults'] > 0) {
                $trophyTitles = $ownedGames['trophyTitles'];
                $arrayPSNGames = array();
                foreach($trophyTitles as $game) {
                    if(array_key_exists('comparedUser', $game)) {
                        array_push($arrayPSNGames, $game);
                    }
                }  
            } else {
                $arrayPSNGames = array();
            }
   
        }

        //Load Xbox Games
        if($user->getXboxID() != null) {
            $xboxId = $user->getXboxID();

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header' => "X-Auth: 682706873075be49a641026c46da7b29acbb53c7"
                    )
                ));

            $getOwnedGames360 = @file_get_contents("https://xboxapi.com/v2/".$xboxId."/xbox360games", false, $context);
            $ownedGames360 = json_decode($getOwnedGames360, true);

            $getOwnedGamesOne = @file_get_contents("https://xboxapi.com/v2/".$xboxId."/xboxonegames", false, $context);
            $ownedGamesOne = json_decode($getOwnedGamesOne, true);

            if($ownedGames360['pagingInfo']['totalRecords'] > 0) {
                $arrayXbox360Games = array(); 
                foreach($ownedGames360['titles'] as $game) {
                    if($game['titleType'] != 5) {
                        array_push($arrayXbox360Games, $game);
                    }
                }
            } else {
                $arrayXbox360Games = array();
            }

            if($ownedGamesOne['pagingInfo']['totalRecords'] > 0) {
                $arrayXboxOneGames = array(); 
                foreach($ownedGamesOne['titles'] as $game) {
                    if($game['titleType'] != 5) {
                        array_push($arrayXboxOneGames, $game);
                    }
                }  
            } else {
                $arrayXboxOneGames = array();
            }

            $arrayTotalXboxGames = array_merge($arrayXboxOneGames, $arrayXbox360Games);
        }

        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
                'user' => $user, 'steamGames' => $arraySteamGames, 'psnGames' => $arrayPSNGames, 'xboxGames' => $arrayTotalXboxGames, "randomPhrases" => $gameNotPlayedPhrases,
        ));
    }

    //Create and render the form to save the Steam ID
    public function addSteamIDAction(Request $request) {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('add_steamID'))
            ->setMethod('POST')
            ->add('type', ChoiceType::class, array(
                    'choices' => array('CurrentURL' => "currentURL", 'SteamID' => "steamid"),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => true,
                ))
            ->add('steamID', TextType::class, array('label' => false, 'attr' => array('placeholder' => 'CurrentURL or SteamID')))
            ->getForm();

        return $this->render('Forms/addSteamIDForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    //Create and render the form to save the PSN ID
    public function addPSNIDAction(Request $request) {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('add_psnID'))
            ->setMethod('POST')
            ->add('psnID', TextType::class, array('label' => 'PSN ID', 'attr' => array('placeholder' => 'PSN ID')))
            ->getForm();

        return $this->render('Forms/addPSNIDForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    //Create and render the form to save the Xbox ID
    public function addXboxIDAction(Request $request) {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('add_xboxID'))
            ->setMethod('POST')
            ->add('xboxID', TextType::class, array('label' => 'Gamertag', 'attr' => array('placeholder' => 'Your gamertag')))
            ->getForm();

        return $this->render('Forms/addXboxIDForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
