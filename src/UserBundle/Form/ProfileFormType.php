<?php

namespace UserBundle\Form;

use FOS\UserBundle\Form\Type\ProfileFormType as ProfileFormBaseType;
use Symfony\Component\Form\FormBuilderInterface; 
use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Form type for the edit
 *
 * @author Galo Drouet  <galo1100@gmail.com>
 * @author Jordi Guerrero <jordiguemo@gmail.com>
 * @author Raul Garcia <raul.garcia.santiago93@gmail.com>
 */

class ProfileFormType extends ProfileFormBaseType {
	
    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options) {
        $builder
			->add('avatar', FileType::class, arraY('label' => 'Avatar', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('firstName', TextType::class, arraY('label' => 'First Name', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('lastName', TextType::class, arraY('label' => 'Last Name', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('nationality', TextType::class, array('label' => 'Nationality', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('about', TextareaType::class, arraY('label' => 'About', 'translation_domain' => 'FOSUserBundle', 'required' => false))
            ->add('avatarUpdatedAt', DateTimeType::class, arraY('label' => ' ', 'data'=> new \DateTime(), 'attr' => array('class' => 'hideType')));
    }
    
     public function getName() {
        return 'fos_user_profile';
    }
}
